import random
def random_line(afile):
    line = next(afile)
    for num, aline in enumerate(afile, 2):
      if random.randrange(num): continue
      line = aline
    return line
username_field = '' + str(random_line(open('names.txt','r'))).strip() + "_" + str(random_line(open('surnames.txt','r')).strip()+"_")
print(username_field)